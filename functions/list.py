import json
import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')  # connect to db


def list(event, context):
    table = dynamodb.Table('cv_950522_todos')  # connect to table
    results = table.scan()  # get all items (todos in the db)

    response = {
        'statusCode': 200,
        'body': json.dumps(results['Items'])  # return all todos
    }

    return response
