import boto3
import json
import uuid

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')


def create(event, context):
    if 'todos_text' not in event['queryStringParameters'].keys():
        return dict(statusCode=200, body=json.dumps({'error': 'cant create todos without text'}))

    # connection to the dynamodb table
    table = dynamodb.Table('cv_950522_todos')

    # what we're going to put in the table (our todo)
    item = {
        'todosId': str(uuid.uuid1()),
        'todos_text': event['queryStringParameters']['todos_text']
    }

    # put it in the table
    table.put_item(Item=item)

    # create a response
    response = {
        'statusCode': 200,
        'body': json.dumps(item)  # will print the created todo
    }

    return response

