import json
import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')  # connect to db


def delete(event, context):
    table = dynamodb.Table('cv_950522_todos')  # connect to table
    table.delete_item(
        Key={
            'todosId': str(event['pathParameters']['id'])
        }
    )

    response = {
        'statusCode': 200,
        'body':  json.dumps('deleted todo')
    }

    return response
