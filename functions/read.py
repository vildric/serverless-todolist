import json
import boto3


dynamodb = boto3.resource('dynamodb', region_name='us-east-1')   # connect to db


def read(event, context):
    table = dynamodb.Table('cv_950522_todos')  # connect to table
    result = table.get_item(  # get item from id in the url
        Key={
            'todosId': str(event['pathParameters']['id'])
        }
    )

    response = {  # return the text from the todo in the database
        'statusCode': 200,
        'body': json.dumps(result['Item']['todos_text'])
    }

    return response
