import json
import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')


def update(event, context):
    table = dynamodb.Table('cv_950522_todos')

    result = table.update_item(
        Key={
            'todosId': str(event['pathParameters']['id'])
        },
        UpdateExpression='set todos.todos_text = :t',
        ExpressionAttributeValues={
            ':t': event['queryStringParameters']['todos_text']
        },
        ReturnValues='UPDATED_NEW'
    )

    response = {
        'statusCode': 200,
        'body': json.dumps(result)
    }

    return response

